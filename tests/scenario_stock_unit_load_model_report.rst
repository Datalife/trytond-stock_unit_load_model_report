=====================================
Stock Unit Load Model Report Scenario
=====================================

Imports::

    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()

Install stock_unit_load_model_report::

    >>> config = activate_modules('stock_unit_load_model_report')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Duplicate UL report::

    >>> ActionReport = Model.get('ir.action.report')
    >>> areport, = ActionReport.find([
    ...     ('report_name', '=', 'stock.unit_load.label')])
    >>> areport2, = areport.duplicate()
    >>> areport2.name = 'A second UL label'
    >>> areport2.report_name = 'stock.unit_load.label'
    >>> areport2.report = areport.report
    >>> areport2.save()

Configure reports for model Unit load::

    >>> IrModel = Model.get('ir.model')
    >>> ul_model, = IrModel.find([
    ...     ('model', '=', 'stock.unit_load')])
    >>> ul_report1 = ul_model.reports.new()
    >>> ul_report1.action_report = areport
    >>> ul_report1.sequence = 10
    >>> ul_report1.save()
    >>> ul_report2 = ul_model.reports.new()
    >>> ul_report2.action_report = areport2
    >>> ul_report2.sequence = 20
    >>> ul_report2.save()

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})

Print reports::

    >>> print_ = Wizard('stock.unit_load.label.print', [ul])
    >>> print_.actions[0][-1].split('-')[0]
    'Label'
    >>> ul_report2.sequence = 5
    >>> ul_report2.save()
    >>> print_ = Wizard('stock.unit_load.label.print', [ul])
    >>> print_.actions[0][-1].split('-')[0]
    'A second UL label'
