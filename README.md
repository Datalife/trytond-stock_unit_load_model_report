datalife_stock_unit_load_model_report
=====================================

The stock_unit_load_model_report module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_unit_load_model_report/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_unit_load_model_report)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
